LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= main.cpp

LOCAL_SHARED_LIBRARIES :=  liblog libdl libutils libcamera_client libbinder libcutils libhardware libui liboverlay

LOCAL_MODULE:= libsurfaceflinger_client

include $(BUILD_SHARED_LIBRARY)
